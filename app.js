var firebaseConfig = {
    apiKey: "AIzaSyAE_-fFhazJT5W_2SYqOLKaDVRim5xIBP0",
    authDomain: "minustic1.firebaseapp.com",
    databaseURL: "https://minustic1.firebaseio.com",
    projectId: "minustic1",
    storageBucket: "minustic1.appspot.com",
    messagingSenderId: "515424928502",
    appId: "1:515424928502:web:631cedc6d6d9413aa8e2d7",
    measurementId: "G-5GN734XV5P"
};
firebase.initializeApp(firebaseConfig);

function saveData() {
    var username = document.getElementById("u_username");
    var email = document.getElementById("u_email");
    var password = document.getElementById("u_password");
    var firebaseRef = firebase.database().ref("User");
    firebaseRef.child(username.value).set("root");
    firebaseRef.child(username.value).child("email").set(email.value);
    firebaseRef.child(username.value).child("password").set(password.value);
    firebaseRef.child(username.value).child("numbOfGlass").set(0);
    signUp();

}
function signUp() {
    var email = document.getElementById("s_email");
    var password = document.getElementById("s_password");
    firebase.auth().createUserWithEmailAndPassword(email.value, password.value).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/weak-password') {
            alert("The password is too weak");
        } else {
            alert(errorMessage);
        }
    }).then(function (sendEmailVerify) {
        if (sendEmailVerify === false) {
            return false;
        } else {
            firebase.auth().currentUser.sendEmailVerification();
            alert("Email Verification Sent! Please check your email address.");
            window.location = "index.html";
            return true;
        }
    })
}

function signIn() {
    var email = document.getElementById("s_email");
    var password = document.getElementById("s_password");
    firebase.auth().signInrWithEmailAndPassword(email.value, password.value).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
            alert("Wrong password");
        } else {
            alert(errorMessage);
        }
    });
}

function showData() {
    var data = document.getElementById("currentData1");
    var txt = "";
    var query = firebase.database().ref("User").orderByKey();
    query.once("value")
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                // key will be "ada" the first time and "alan" the second time
                var key = childSnapshot.key;
                // childData will be the actual contents of the child

                txt += key + ", "
            });
            data.innerHTML = "All user: " + txt
        });
}
function addNumsGlass() {
    var numb = 0;
    var result = 0;
    var value = document.getElementById("value").value;
    var numbGlass = document.getElementById("numbGlass");
    var firebaseRef = firebase.database().ref("User");
    var numbRef = firebaseRef.child("unm").child("numbOfGlass");
    numbRef.once("value").then(function (dataSnapshot) {
        numb = dataSnapshot.val();
        result = numb + parseInt(value);
        numbGlass.innerHTML = "glass number: " + result
        numbRef.set(result);
    });

}
function saveStoreData() {
    var username = document.getElementById("s_name");
    var email = document.getElementById("s_email");
    var password = document.getElementById("s_password");
    var glassPrice = document.getElementById("glassPrice");
    var firebaseRef = firebase.database().ref("Store");
    firebaseRef.child(username.value).set("root");
    firebaseRef.child(username.value).child("email").set(email.value);
    firebaseRef.child(username.value).child("password").set(password.value);
    firebaseRef.child(username.value).child("glassPrice").set(parseInt(glassPrice.value));
    signUp();

}

function showStoreData() {
    var data = document.getElementById("currentData2");
    var txt = "<ol>";
    var query = firebase.database().ref("Store").orderByKey();
    query.once("value")
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                // key will be "ada" the first time and "alan" the second time
                var key = childSnapshot.key;
                // childData will be the actual contents of the child

                txt += "<li>" + key + "</li>"
            });
            txt += "</ol>"
            document.getElementById("store_name").innerHTML = txt
        });
}
function saveCPData() {
    var project = document.getElementById("project");
    var object = document.getElementById("object");
    var desp = document.getElementById("description");
    var place = document.getElementById("place");
    var budget = document.getElementById("budget");
    var underTaker = document.getElementById("undertaker");
    var c_phone = document.getElementById("c_phone");
    var c_email = document.getElementById("c_email");
    var firebaseRef = firebase.database().ref("CreativeProject");
    firebaseRef.child(project.value).set("root");
    firebaseRef.child(project.value).child("object").set(object.value);
    firebaseRef.child(project.value).child("desp").set(desp.value);
    firebaseRef.child(project.value).child("place").set(place.value);
    firebaseRef.child(project.value).child("budget").set(budget.value);
    firebaseRef.child(project.value).child("underTaker").set(underTaker.value);
    firebaseRef.child(project.value).child("c_phone").set(c_phone.value);
    firebaseRef.child(project.value).child("c_email").set(c_email.value);
    alert("OK")
    //signUp();

}

function showCPData() {
    var data = document.getElementById("currentData3");
    var txt = "<ul>";
    var query = firebase.database().ref("CreativeProject").orderByKey();
    query.once("value")
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                // key will be "ada" the first time and "alan" the second time
                var key = childSnapshot.key;
                // childData will be the actual contents of the child

                txt += "<li>" + key + "</li>"
            });
            txt += "</ul>"
            data.innerHTML = "All project: " + txt
        });
}

function showNumbOfGlassData() {
    var data = document.getElementById("currentData4");
    var firebaseRef = firebase.database().ref("User");
    var numb = 0;
    var query = firebase.database().ref("User").orderByKey();
    query.once("value")
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                // key will be "ada" the first time and "alan" the second time
                var nkey = childSnapshot.key;
                console.log(nkey)
                // childData will be the actual contents of the child
                var numbRef = firebaseRef.child(nkey).child("numbOfGlass");
                numbRef.once("value").then(function (dataSnapshot) {

                    numb += dataSnapshot.val();
                    console.log("each " + dataSnapshot.val())
                    console.log(numb)
                    // console.log(dataSnapshot.val())
                    data.innerHTML = "ALL IS  " + numb
                });

console.log(numb)
            });

        })
}

function userRanking() {
    var data = document.getElementById("currentData5");
    var values = [];
    var top = [];
    var check = true;
    var firebaseRef = firebase.database().ref("User");
    var query = firebase.database().ref("User").orderByKey();
    if (check) {
        query.once("value")
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var nkey = childSnapshot.key;
                    var numbRef = firebaseRef.child(nkey).child("numbOfGlass");
                    numbRef.once("value").then(function (dataSnapshot) {
                        values.push(dataSnapshot.val());
                        topValues = values.sort((a, b) => b - a).slice(0, 3);
                        console.log("out loop: " + values)
                        values.length = 3;
                        query.once("value")
                            .then(function (snapshot) {
                                snapshot.forEach(function (childSnapshot) {
                                    var userkey = childSnapshot.key;
                                    var numbRef2 = firebaseRef.child(userkey).child("numbOfGlass");
                                    numbRef2.once("value").then(function (dataSnapshot) {
                                        if (values.includes(dataSnapshot.val())) {
                                            if (top.indexOf(userkey) === -1) {
                                                console.log("not duplicate!");
                                                if (values[0] === dataSnapshot.val()) {
                                                    top.splice(0, 0, userkey);
                                                } else if (values[1] === dataSnapshot.val()) {
                                                    top.splice(1, 0, userkey);
                                                } else if (values[2] === dataSnapshot.val()) {
                                                    top.splice(2, 0, userkey);
                                                }
                                                console.log("in loop: " + values)
                                                console.log("user: " + top)
                                                
                                                console.log("beforloop: " + top[0])
                                                console.log("legnth: " + values.length)
                                                
                                            }
                                            var list = "<ol>"
                                            console.log("***bein for loop ")
                                            console.log(top[2])
                                            console.log(values[2])
                                            for(var i = 0 ; i < 3 ; i++){
                                                console.log("***in for loop " + i)
                                                list += "<li>" + top[i] + " : " + values[i] + "</li>"
                                                console.log(top[i])
                                                console.log(values[i])

                                            }
                                            list += "</ol>"
                                           
                                            document.getElementById("userRank").innerHTML = list


                                        }
                                    });
                                });
                            })

                    })

                    check = false
                })
            })
    }
}

function storeRanking() {
    var data = document.getElementById("currentData6");
    var values = [];
    var top = [];
    var check = true;
    var firebaseRef = firebase.database().ref("Store");
    var query = firebase.database().ref("Store").orderByKey();
    if (check) {
        query.once("value")
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var nkey = childSnapshot.key;
                    var numbRef = firebaseRef.child(nkey).child("numbOfGlass");
                    numbRef.once("value").then(function (dataSnapshot) {
                        values.push(dataSnapshot.val());
                        topValues = values.sort((a, b) => b - a).slice(0, 3);
                        console.log("out loop: " + values)
                        values.length = 3;
                        query.once("value")
                            .then(function (snapshot) {
                                snapshot.forEach(function (childSnapshot) {
                                    var userkey = childSnapshot.key;
                                    var numbRef2 = firebaseRef.child(userkey).child("numbOfGlass");
                                    numbRef2.once("value").then(function (dataSnapshot) {
                                        if (values.includes(dataSnapshot.val())) {
                                            if (top.indexOf(userkey) === -1) {
                                                
                                                if (values[0] === dataSnapshot.val()) {
                                                    top.splice(0, 0, userkey);
                                                } else if (values[1] === dataSnapshot.val()) {
                                                    top.splice(1, 0, userkey);
                                                } else if (values[2] === dataSnapshot.val()) {
                                                    top.splice(2, 0, userkey);
                                                }
                                
                                            }
                                            var list = "<ol>"
                                            for(var i = 0 ; i < 3 ; i++){
                                               
                                                list += "<li>" + top[i] + " : " + values[i] + "</li>"
                                              

                                            }
                                            list += "</ol>"
                                            document.getElementById("storeRank").innerHTML = list


                                        }
                                    });
                                });
                            })

                    })

                    check = false
                })
            })
    }
}

function numAndpriceOfGlass() {
    var storeName = document.getElementById("storeName");
    var numbOfGlass = document.getElementById("glassNum");
    var total1 = 0;
    var total = 0;
    var totalMoney = 0;
   
    var firebaseRef = firebase.database().ref("Store");
    var numbRef = firebaseRef.child(storeName.value).child("glassPrice");
    numbRef.once("value").then(function (dataSnapshot) {
        total = parseInt(numbOfGlass.value) * parseInt(dataSnapshot.val());
        data.innerHTML = "total price " + total
    });
    var firebaseRef = firebase.database().ref("Fund");
    var numbRef = firebaseRef.child("totalMoney");
    numbRef.once("value").then(function (dataSnapshot) {
        totalMoney = total + parseInt(dataSnapshot.val());
        numbRef.set(totalMoney);
    });
    var firebaseRef = firebase.database().ref("Fund");
    var numbRef = firebaseRef.child("totalGlass");
    numbRef.once("value").then(function (dataSnapshot) {
    total1 = parseInt(numbOfGlass.value) + parseInt(dataSnapshot.val());
    numbRef.set(total1);
    console.log("num " + total1) })
}

function showTotalGlass() {
    var firebaseRef = firebase.database().ref("Fund");
    var numbRef = firebaseRef.child("totalGlass");
        numbRef.once("value").then(function (dataSnapshot) {
        
        document.getElementById("showGlass").innerHTML = dataSnapshot.val()
    })
    
}
function showTotalMoney() {
    var firebaseRef = firebase.database().ref("Fund");
    var numbRef = firebaseRef.child("totalMoney");
        numbRef.once("value").then(function (dataSnapshot) {
       
        document.getElementById("showMoney").innerHTML = dataSnapshot.val()
    })
    
}
function showTotalCP() {
    var txt = "<ul>";
    var query = firebase.database().ref("CreativeProject").orderByKey();
    query.once("value")
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                // key will be "ada" the first time and "alan" the second time
                var nkey = childSnapshot.key;
                // childData will be the actual contents of the child
                console.log(nkey)

                txt += "<li>" + key 
                var firebaseRef = firebase.database().ref("CreativeProject");
                var numbRef = firebaseRef.child(nkey).child("desp");
                numbRef.once("value").then(function (dataSnapshot) {
                    numb = dataSnapshot.val();
                    txt += numb + "</li>"
                  
                });
            
            });
            txt = "</ul>"
            console.log(txt)
       // document.getElementById("con1").innerHTML = txt
            
        });
        console.log("txt" + txt)
        document.getElementById("con1").innerHTML = "kkk"
    
}

showTotalGlass();
showTotalMoney();
userRanking();
storeRanking();
showStoreData();
showTotalCP();

